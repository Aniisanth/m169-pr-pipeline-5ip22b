# M169 Prüfung Pipeline

## Situation

Ihr Supportteam betreibt eine kleine statische Website, auf der sie jeweils am Freitag das Wochenmenü der Mensa der Folgewoche publizieren. Dieses Menü wird auch als PDF-Datei zur Verfügung gestellt. Sie erstellen nun einen Automat (per gitlab Pipeline), der aus einer einzupflegenden Markdown-Datei das PDF erstellt. Dieses wird als *Artifact* (*Build* -> *Artifact*) der weiteren Verarbeitung zur Verfügung gestellt.

## Aufgaben

1. Forken Sie dieses Projekt! Arbeiten Sie öffentlich. [1 Punkt]

1. Verlinken Sie Ihren eigenen Runnner ins Projekt. [2 Punkte]

1. Clonen Sie Ihr Projekt lokal. [0 Punkte]

1. Erstellen Sie eine Datei `name.txt`, welche Ihren Namen enthält [0 Punkte]

1. Erstellen Sie eine Pipeline. [7 Punkte]

    - Die Pipeline soll mittels *pandoc* / *latex* aus dem bestehenden `menu.md` ein PDF erstellen. Der Befehl dazu lautet
      ```
      pandoc menu.md -V "geometry:landscape,margin=1cm" -o menu.pdf
      ```
      Für die erfolgreiche Ausführung benötigen Sie die Pakete `pandoc` und `texlive`.

    - Die Datei `menu.pdf` wollen wir als *Artifact* verfügbar machen. Hilfe zum Gebrauch von Artifacts finden Sie z. B. im [GitLab-Handbuch](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab7/).

    - Sorgen Sie dafür, dass das PDF 1 Woche als *Artifact* bestehen bleibt. [2 Punkte]

1. Nach Durchführung des Jobs analysieren Sie diesen. Erstellen Sie eine Datei `fazit.txt`, in welche Sie in der ersten Zeile die genaue Laufzeit des Jobs notieren. [1 Punkt] Schreiben Sie dazu, wie diese Zeit zustande kommt (was benötigt so viel Zeit?). [2 Punkte]

1. Erstellen Sie eine Datei `optimierung.txt`, in der Sie einen Vorschlag unterbreiten, wie diese Zeit reduziert werden könnte. Um das Szenario zu verschärfen, gehen wir davon aus, dass die Laufzeit mit > 1 Min hoch, Rechenzeit sehr teuer und Speicherplatz bei gitlab gratis zur Verfügung steht. Die Pipeline sei jede Minute auszuführen (also nicht mehr unbedingt der Menüplan). Wie liesse sich die Laufzeit stark verringern? Argumentieren Sie konzeptionell (ohne Code). [3 Punkte]

Vergessen Sie nicht, alles sauber in Ihr gitlab zu speichern. Nur was da vorhanden ist, wird in die Bewertung einfliessen. Auch der Abgabe-Zeitpunkt (letzter commit/push) fliesst in die Note ein.

*Viel Erfolg!*
